using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using ImageProcessingUtils;

namespace Assignment7
{
    abstract class RescalingImage : IDisposable
    {
        private Bitmap _bitmap;
        private bool _disposed ;

        public RescalingImage(Bitmap bitmap)
        {
            this._bitmap = bitmap;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _bitmap.Dispose();
                }

                _disposed = true;
            }
        }

        ~RescalingImage()
        {
            Dispose(false);
        }
        
        static void Main(string[] args)
        {
            string directory = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            string imagePath = Path.Combine(directory, "image2.jpg");

            Bitmap originalImage = new Bitmap(imagePath);

            Bitmap rescaled = RescaleImage(originalImage, 2000, 1500);

            rescaled.Save("Rescaled.bmp", ImageFormat.Bmp);

            Console.WriteLine("Image rescaled successfully.");

            originalImage.Dispose();
            rescaled.Dispose();

        }
        public static Bitmap RescaleImage(Bitmap image, int newWidth, int newHeight)
        {
            int originalWidth = image.Width;
            int originalHeight = image.Height;

            double scaleFactorX = (double)newWidth / originalWidth;
            double scaleFactorY = (double)newHeight / originalHeight;
            double scaleFactor = Math.Min(scaleFactorX, scaleFactorY);

            int rescaledWidth = (int)(originalWidth * scaleFactor);
            int rescaledHeight = (int)(originalHeight * scaleFactor);

            Bitmap scaledImage = new Bitmap(rescaledWidth, rescaledHeight, PixelFormat.Format24bppRgb);
            
            using (Graphics graphics = Graphics.FromImage(scaledImage))
            {
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                graphics.DrawImage(image, 0, 0, rescaledWidth, rescaledHeight);
            }

            Bitmap grayImage = ImageProcessing.ConvertToGray(scaledImage);

            scaledImage.Dispose();
            return grayImage;
        }
    }
}
