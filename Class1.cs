﻿using System.Drawing;
using System.Drawing.Imaging;

namespace ImageProcessingUtils
{
    public static class ImageProcessing
    {
        public static Bitmap ConvertToGray(Bitmap image)
        {
            if (image.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                return image;
            }

            int width = image.Width;
            int height = image.Height;

            Bitmap convertedImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

            ColorPalette palette = convertedImage.Palette;
            for (int i = 0; i < 256; i++)
            {
                palette.Entries[i] = Color.FromArgb(i, i, i);
            }

            convertedImage.Palette = palette;

            BitmapData convertedImageData = convertedImage.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            BitmapData imageData = image.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);

            int originalOffset = imageData.Stride - width * 3;
            int convertedOffset = convertedImageData.Stride - width;

            unsafe
            {
                byte* originalPointer = (byte*)imageData.Scan0;
                byte* convertedImagePointer = (byte*)convertedImageData.Scan0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        byte r = originalPointer[2];
                        byte g = originalPointer[1];
                        byte b = originalPointer[0];

                        byte gray = (byte)(0.3 * r + 0.59 * g + 0.11 * b);
                        convertedImagePointer[0] = gray;

                        originalPointer += 3;
                        convertedImagePointer += 1;
                    }

                    originalPointer += originalOffset;
                    convertedImagePointer += convertedOffset;
                }
            }

            image.UnlockBits(imageData);
            convertedImage.UnlockBits(convertedImageData);
            image.Dispose();
            return convertedImage;
        }
        
        public static Bitmap Convert1bppToGray(Bitmap image)
        {
            int width = image.Width;
            int height = image.Height;
            Bitmap convertedImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

            ColorPalette palette = convertedImage.Palette;
            for (int i = 0; i < 256; i++)
            {
                palette.Entries[i] = Color.FromArgb(i, i, i);
            }

            convertedImage.Palette = palette;

            BitmapData convertedImageData = convertedImage.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            BitmapData originalImageData = image.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadOnly, PixelFormat.Format1bppIndexed);

            unsafe
            {
                byte* originalPointer = (byte*)originalImageData.Scan0;
                byte* convertedPointer = (byte*)convertedImageData.Scan0;

                int originalStride = originalImageData.Stride;
                int convertedStride = convertedImageData.Stride;

                for (int y = 0; y < height; y++)
                {
                    byte* originalRow = originalPointer + y * originalStride;
                    byte* convertedRow = convertedPointer + y * convertedStride;

                    for (int i = 0; i < width / 8; i++)
                    {
                        for (int b = (1<<7); b > 0; b >>= 1)
                        {
                            *convertedRow++ = (byte)((originalRow[i] & b) * 255);
                        }
                    }
                }
            }
            image.UnlockBits(originalImageData);
            convertedImage.UnlockBits(convertedImageData);
            image.Dispose();

            return convertedImage;
        }
    }
}
